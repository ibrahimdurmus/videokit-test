//
//  AppDelegate.h
//  VideoKitTest
//
//  Created by ibrahim on 17.01.2017.
//  Copyright © 2017 Patriot-1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

