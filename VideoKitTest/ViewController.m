//
//  ViewController.m
//  VideoKitTest
//
//  Created by ibrahim on 17.01.2017.
//  Copyright © 2017 Patriot-1. All rights reserved.
//

#import "ViewController.h"
#import "VKPlayerViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    /*
     **RTSP için alternatif option
     */
    //NSDictionary *option = [NSDictionary dictionaryWithObject:VKDECODER_OPT_VALUE_RTSP_TRANSPORT_TCP forKey:VKDECODER_OPT_KEY_RTSP_TRANSPORT];
    
    NSString *urlString = @"";
    
    VKPlayerViewController *playerVC = [[VKPlayerViewController alloc] initWithURLString:urlString decoderOptions:nil];
    
    [self presentViewController:playerVC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
