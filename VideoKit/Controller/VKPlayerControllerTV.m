//
//  VKPlayerViewControllerTV.m
//  VideoKitSample
//
//  Created by Murat Sudan on 23/09/15.
//  Copyright © 2015 iosvideokit. All rights reserved.
//

#import "VKPlayerControllerTV.h"
#import "VKGLES2View.h"
#import "VKStreamInfoView.h"
#import "VKFullscreenContainer.h"

@interface FocusableView : UIView

@property (nonatomic, assign) BOOL focusToSlider;
@property (nonatomic, assign) BOOL ignoreFocusAnimations;

@end

@implementation FocusableView

@synthesize focusToSlider = _focusToSlider;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Custom initialization
        return self;
    }
    return nil;
}

#pragma mark - Focus Management

- (UIView *)preferredFocusedView {
    
    if (_focusToSlider) {
        
        UIView *slider = nil;
        
        for (UIView *v in self.subviews) {
            for (UIView *z in v.subviews) {
                if ([z isKindOfClass:[FGThrowSlider class]]) {
                    slider = z;
                }
            }
        }
        
        if (slider) {
            VKLog(kVKLogLevelUIControlExtra,@"Player view: preferredFocusedView-> slider");
            return slider;
        }
    }
    
    VKLog(kVKLogLevelUIControlExtra,@"Player view: preferredFocusedView-> self");
    return self;
}

- (BOOL)canBecomeFocused {
    if (!_focusToSlider) {
        VKLog(kVKLogLevelUIControlExtra,@"Player view: canBecomeFocused-> YES");
        return YES;
    }
    
    UIView *slider = nil;
    
    for (UIView *v in self.subviews) {
        for (UIView *z in v.subviews) {
            if ([z isKindOfClass:[FGThrowSlider class]]) {
                slider = z;
            }
        }
    }
    
    if (slider && slider.isHidden) {
        VKLog(kVKLogLevelUIControlExtra,@"Player view: canBecomeFocused-> YES");
        return YES;
    }
    
    
    VKLog(kVKLogLevelUIControlExtra,@"Player view: canBecomeFocused-> NO");
    return NO;
}


- (void) didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator {
    [coordinator addCoordinatedAnimations:^{
        if (!_ignoreFocusAnimations) {
            if (self.focused) {
                VKLog(kVKLogLevelUIControlExtra,@"Player view: Focused");
                self.layer.masksToBounds = NO;
                self.layer.shadowColor = [[UIColor blackColor] CGColor];
                self.layer.shadowRadius = 30;
                self.layer.shadowOpacity = 1.0;
            } else {
                VKLog(kVKLogLevelUIControlExtra,@"Player view: Not Focused");
                self.layer.masksToBounds = YES;
                self.layer.shadowRadius = 0;
                self.layer.shadowOpacity = 0.0;
            }
        }
    } completion:nil];
}

@end

@interface VKPlayerControllerTV () {

#ifdef VK_RECORDING_CAPABILITY
    UILabel *_labelRecording;
#endif
    FGThrowSlider *_slider;
    UILabel *_labelStreamCurrentTime;
    UILabel *_labelStreamTotalDuration;
    
    UIView *_viewBottomPanel;
    UIView *_viewTopPanel;
    UILabel *_labelTopTitle;
    UIActivityIndicatorView *_activityIndicator;
    
    UITapGestureRecognizer *_homeTapRecognizer;
    UITapGestureRecognizer *_pauseTapRecognizer;
    
    NSTimer *timerForRecordingLabel;
}

@end

@implementation VKPlayerControllerTV

@synthesize controlStyle = _controlStyle;
@synthesize fullScreenAnimationInProgress = _fullScreenAnimationInProgress;


#pragma mark Initialization

- (id)init {
    self = [super initBase];
    if (self) {
        [self prepare];
        return self;
    }
    return nil;
}

- (id)initWithURLString:(NSString *)urlString {
    
    self = [super initWithURLString:urlString];
    if (self) {
        [self prepare];
        return self;
    }
    return nil;
}

- (void)prepare {
    // Custom initialization
    _controlStyle = kVKPlayerControlStyleTVDefault;
    [self createUI];
    [self addAppleTVRemoteControllerGestures];
}

#pragma mark Subviews initialization

- (void)createUI {
    _view = [[FocusableView alloc] initWithFrame:CGRectZero];
    self.view.backgroundColor = _backgroundColor;
    
    [self createUITopPanel];
    [self createUIBottomPanel];
    [self createUICenter];
}

- (void)createUITopPanel {

    
    /* Toolbar on top: _viewTopPanel */
    _viewTopPanel = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
    _viewTopPanel.translatesAutoresizingMaskIntoConstraints = NO;
    _viewTopPanel.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    [self.view addSubview:_viewTopPanel];

    
    // height og top panel is 70
    [_viewTopPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_viewTopPanel(==70)]"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings(_viewTopPanel)]];
    
    
    
    // align _viewTopPanel from the left and right
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[_viewTopPanel]-0-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(_viewTopPanel)]];
    

    // align _viewTopPanel from the top
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[_viewTopPanel]"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(_viewTopPanel)]];
    

    

    /* Toolbar on top: _labelTopTitle */
    _labelTopTitle = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    _labelTopTitle.translatesAutoresizingMaskIntoConstraints = NO;
    _labelTopTitle.contentMode = UIViewContentModeCenter;
    _labelTopTitle.lineBreakMode = NSLineBreakByTruncatingTail;
    _labelTopTitle.minimumScaleFactor = 0.6;
    _labelTopTitle.textAlignment = NSTextAlignmentCenter;
    _labelTopTitle.numberOfLines = 1;
    _labelTopTitle.backgroundColor = [UIColor clearColor];
    _labelTopTitle.shadowOffset = CGSizeMake(0.0, -1.0);
    _labelTopTitle.textColor = [UIColor darkGrayColor];
    _labelTopTitle.font = [UIFont fontWithName:@"HelveticaNeue" size:35];
    [_viewTopPanel addSubview:_labelTopTitle];

    // align _labelTopTitle from top and bottom
    [_viewTopPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[_labelTopTitle]-0-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(_labelTopTitle)]];
    
    // center _labelTopTitle horizontally
    [_viewTopPanel addConstraint:[NSLayoutConstraint constraintWithItem:_labelTopTitle
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:_viewTopPanel
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.0
                                                               constant:0]] ;
    
    
    /* Toolbar on top: _activityIndicator */
    _activityIndicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite] autorelease];
    _activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    _activityIndicator.hidesWhenStopped = YES;
    [_viewTopPanel addSubview:_activityIndicator];
    

    // align _activityIndicator from the left
    [_viewTopPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_activityIndicator]-60-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(_activityIndicator)]];
    
    // center _activityIndicator vertically
    [_viewTopPanel addConstraint:[NSLayoutConstraint constraintWithItem:_activityIndicator
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:_viewTopPanel
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1.0
                                                               constant:0]] ;
    
    // align _labelTopTitle - _activityIndicator
    [_viewTopPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_labelTopTitle]-30-[_activityIndicator]"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(_labelTopTitle, _activityIndicator)]];

}

- (void)createUIBottomPanel {

    
    _viewBottomPanel = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
    _viewBottomPanel.translatesAutoresizingMaskIntoConstraints = NO;
    _viewBottomPanel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_viewBottomPanel];

    // height of bottom panel is 120
    [_viewBottomPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_viewBottomPanel(==100)]"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(_viewBottomPanel)]];
    
    
    
    // align _viewTopPanel from the left and right
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[_viewBottomPanel]-0-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(_viewBottomPanel)]];
    
    
    // align _viewTopPanel from the top
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_viewBottomPanel]-0-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(_viewBottomPanel)]];
    
    
    
    _slider = [FGThrowSlider sliderWithFrame:CGRectZero andDelegate:self];
    _slider.translatesAutoresizingMaskIntoConstraints = NO;
    _slider.backgroundColor = [UIColor clearColor];
    _slider.hidden = YES;
    [_viewBottomPanel addSubview:_slider];
    
    [_slider addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_slider(==50)]"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(_slider)]];
    
    // center _slider in x-axis
    [_viewBottomPanel addConstraint:[NSLayoutConstraint constraintWithItem:_slider
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:_viewBottomPanel
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.0
                                                               constant:0]] ;
    
    // vertical spacing of _slider to container
    [_viewBottomPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[_slider]"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(_slider)]];
    
    
    // create a container for time labels
    UIView *timeLabelContainer = [[UIView alloc] initWithFrame:CGRectZero];
    timeLabelContainer.translatesAutoresizingMaskIntoConstraints = NO;
    timeLabelContainer.backgroundColor = [UIColor clearColor];
    [_viewBottomPanel addSubview:timeLabelContainer];
    
    // vertical spacing of timeLabelContainer to _slider
    [_viewBottomPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_slider]-(-10)-[timeLabelContainer]|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(_slider, timeLabelContainer)]];
    
    // width of timeLabelContainer
    [_viewBottomPanel addConstraint:[NSLayoutConstraint constraintWithItem:timeLabelContainer
                                                                 attribute:NSLayoutAttributeWidth
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:_viewBottomPanel
                                                                 attribute:NSLayoutAttributeWidth
                                                                multiplier:0.65
                                                                  constant:0]];

    // center timeLabelContainer in x-axis
    [_viewBottomPanel addConstraint:[NSLayoutConstraint constraintWithItem:timeLabelContainer
                                                                  attribute:NSLayoutAttributeCenterX
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:_viewBottomPanel
                                                                  attribute:NSLayoutAttributeCenterX
                                                                 multiplier:1.0
                                                                   constant:0]] ;
    
    // _slider width is equal to timeLabelContainer
    [_viewBottomPanel addConstraint:[NSLayoutConstraint constraintWithItem:_slider
                                                                 attribute:NSLayoutAttributeWidth
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:timeLabelContainer
                                                                 attribute:NSLayoutAttributeWidth
                                                                multiplier:1.0
                                                                  constant:0]] ;

    
    _labelStreamCurrentTime = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    _labelStreamCurrentTime.translatesAutoresizingMaskIntoConstraints = NO;
    _labelStreamCurrentTime.textAlignment = NSTextAlignmentLeft;
    _labelStreamCurrentTime.text = @"00:00";
    _labelStreamCurrentTime.numberOfLines = 1;
    _labelStreamCurrentTime.opaque = NO;
    _labelStreamCurrentTime.backgroundColor = [UIColor clearColor];
    _labelStreamCurrentTime.font = [UIFont fontWithName:@"HelveticaNeue" size:35.0];
    _labelStreamCurrentTime.textColor = [UIColor whiteColor];
    _labelStreamCurrentTime.shadowOffset = CGSizeMake(0.0, -1.0);
    _labelStreamCurrentTime.shadowColor = [UIColor blackColor];
    _labelStreamCurrentTime.minimumScaleFactor = 0.3;
    _labelStreamCurrentTime.adjustsFontSizeToFitWidth = YES;
    _labelStreamCurrentTime.hidden = YES;
    
    [timeLabelContainer addSubview:_labelStreamCurrentTime];

    CGFloat leftOrRightSpacing = 10.0;
    [timeLabelContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[_labelStreamCurrentTime]-%f-|", leftOrRightSpacing]
                                                                             options:0
                                                                             metrics:nil
                                                                               views:NSDictionaryOfVariableBindings(_labelStreamCurrentTime)]];
    
    [timeLabelContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_labelStreamCurrentTime]"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:NSDictionaryOfVariableBindings(_labelStreamCurrentTime)]];
    
    
    
    /* labelStreamTotalDuration */
    _labelStreamTotalDuration = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    _labelStreamTotalDuration.translatesAutoresizingMaskIntoConstraints = NO;
    _labelStreamTotalDuration.textAlignment = NSTextAlignmentRight;
    _labelStreamTotalDuration.numberOfLines = 1;
    _labelStreamTotalDuration.opaque = NO;
    _labelStreamTotalDuration.backgroundColor = [UIColor clearColor];
    _labelStreamTotalDuration.textColor = [UIColor whiteColor];
    _labelStreamTotalDuration.font = [UIFont fontWithName:@"HelveticaNeue" size:35.0];
    _labelStreamTotalDuration.shadowOffset = CGSizeMake(0.0, -1.0);
    _labelStreamTotalDuration.shadowColor = [UIColor blackColor];
    _labelStreamTotalDuration.minimumScaleFactor = 0.3;
    _labelStreamTotalDuration.adjustsFontSizeToFitWidth = YES;
    _labelStreamTotalDuration.hidden = YES;
    
    [timeLabelContainer addSubview:_labelStreamTotalDuration];
    
    [timeLabelContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[_labelStreamTotalDuration]-%f-|", leftOrRightSpacing]
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:NSDictionaryOfVariableBindings(_labelStreamTotalDuration)]];
    
    
    [timeLabelContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_labelStreamTotalDuration]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:NSDictionaryOfVariableBindings(_labelStreamTotalDuration)]];
    
#ifdef VK_RECORDING_CAPABILITY

    _labelRecording = [[UILabel alloc] initWithFrame:CGRectZero];
    _labelRecording.translatesAutoresizingMaskIntoConstraints = NO;
    _labelRecording.textAlignment = NSTextAlignmentCenter;
    _labelRecording.numberOfLines = 1;
    _labelRecording.opaque = NO;
    _labelRecording.backgroundColor = [UIColor clearColor];
    _labelRecording.textColor = [UIColor redColor];
    _labelRecording.font = [UIFont fontWithName:@"HelveticaNeue" size:40.0];
    _labelRecording.minimumScaleFactor = 0.3;
    _labelRecording.adjustsFontSizeToFitWidth = YES;
    _labelRecording.hidden = YES;
    _labelRecording.text = @"Recording";
    
    [timeLabelContainer addSubview:_labelRecording];
    [timeLabelContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[_labelRecording]-%f-|",
                                                                                        leftOrRightSpacing]
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:NSDictionaryOfVariableBindings(_labelRecording)]];
    
    [timeLabelContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_labelStreamCurrentTime]-5-[_labelRecording]-5-[_labelStreamTotalDuration]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:NSDictionaryOfVariableBindings(_labelStreamCurrentTime, _labelRecording, _labelStreamTotalDuration)]];
    
    [timeLabelContainer addConstraint:[NSLayoutConstraint constraintWithItem:_labelRecording
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:timeLabelContainer
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1.0
                                                                  constant:0]] ;

    
#endif
}

- (void) createUICenter {
    [super createUICenter];
}

#pragma mark - Subviews management

- (void)updateBarWithDurationState:(VKError) state {
    
    BOOL value = NO;
    if (state == kVKErrorNone) {
        value = YES;
    }
    
    [_labelStreamCurrentTime setHidden:!value];
    [_labelStreamTotalDuration setHidden:!value];
    [_slider setHidden:!value];
}

- (void)focusOnSubview:(BOOL)value {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [(FocusableView *)_view setFocusToSlider:(_controlStyle != kVKPlayerControlStyleTVNone && value) ? YES : NO];
        [_view setNeedsFocusUpdate];
        [_view updateFocusIfNeeded];
    });
}

- (void)setControlStyle:(VKPlayerControlStyleTV)controlStyle {
    _controlStyle = controlStyle;
    if (_controlStyle == kVKPlayerControlStyleTVNone) {
        [self showControlPanel:NO willExpire:NO];
    }
}

- (void)setFullScreen:(BOOL)value animated:(BOOL)animated {
    if (_fullScreenAnimationInProgress)
        return;
    
    _fullScreenAnimationInProgress = YES;
    
    if (value && !_fullScreen) {
        _fullScreen = YES;
        [(FocusableView *)_view setIgnoreFocusAnimations:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:kVKPlayerWillEnterFullscreenNotification object:self userInfo:nil];
        if (_containerVc &&
            ([NSStringFromClass([_containerVc class]) isEqualToString:@"VKPlayerViewController"])) {
            [(FocusableView *)_view setFocusToSlider:(_controlStyle != kVKPlayerControlStyleTVNone && !_slider.isHidden) ? YES : NO];
            _controlStyle = kVKPlayerControlStyleTVDefault;
            [[NSNotificationCenter defaultCenter] postNotificationName:kVKPlayerDidEnterFullscreenNotification object:self userInfo:nil];
            _fullScreenAnimationInProgress = NO;
            return;
        } else {
            [self useContainerViewControllerAnimated:animated];
        }
    } else if (!value && _fullScreen) {
        _fullScreen = NO;
        if (_containerVc &&
            ([NSStringFromClass([_containerVc class]) isEqualToString:@"VKPlayerViewController"])) {
            _fullScreenAnimationInProgress = NO;
            return;
        } else {
            if (_containerVc) {
                [(FocusableView *)_view setIgnoreFocusAnimations:NO];
                [[NSNotificationCenter defaultCenter] postNotificationName:kVKPlayerWillExitFullscreenNotification object:self userInfo:nil];
                [(VKFullscreenContainer *)_containerVc dismissContainerWithAnimated:animated completionHandler:^{
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [_containerVc release];
                        _containerVc = nil;
                        
                        if (_controlStyle != kVKPlayerControlStyleTVNone) {
                            _controlStyle = kVKPlayerControlStyleTVDefault;
                        }
                        
                        [self setFullScreenAnimationInProgress:NO];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:kVKPlayerDidExitFullscreenNotification object:self userInfo:nil];
                    });
                }];
            }
        }
    }
}

- (void)useContainerViewControllerAnimated:(BOOL)animated {
    UIViewController *currentVc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    UIViewController *topVc = nil;
    
    if (currentVc) {
        if ([currentVc isKindOfClass:[UINavigationController class]]) {
            topVc = [(UINavigationController *)currentVc topViewController];
        } else if ([currentVc isKindOfClass:[UITabBarController class]]) {
            topVc = [(UITabBarController *)currentVc selectedViewController];
        } else if ([currentVc presentedViewController]) {
            topVc = [currentVc presentedViewController];
        } else if ([currentVc isKindOfClass:[UIViewController class]]) {
            topVc = currentVc;
        } else {
            VKLog(kVKLogLevelDecoder, @"Expected a view controller but not found...");
            return;
        }
    } else {
        VKLog(kVKLogLevelDecoder, @"Expected a view controller but not found...");
        return;
    }
    
    [self.scrollView setZoomScale:1.0 animated:NO];
    [self.scrollView setDisableCenterViewNow:YES];
    
    [self.view.superview bringSubviewToFront:self.view];
    
    float duration = (animated) ? 0.5 : 0.0;
    
    UIWindow *windowActive = [[[UIApplication sharedApplication] windows] lastObject];
    
    CGRect newRectToWindow = [windowActive convertRect:self.view.frame fromView:self.view.superview];
    VKFullscreenContainer *fsContainerVc = [[[VKFullscreenContainer alloc] initWithPlayerController:self
                                                                                         windowRect:newRectToWindow] autorelease];
    fsContainerVc.view.backgroundColor = [UIColor clearColor];
    
    self.view.frame = newRectToWindow;
    [windowActive addSubview:self.view];
    
    __block NSLayoutConstraint *constraintTopByWin = nil, *constraintLeftByWin = nil, *constraintWidthByWin = nil, *constraintHeightByWin = nil;
    
    if (![self.view translatesAutoresizingMaskIntoConstraints]) {
        //Set Autolayout constraints self.view
        
        for (NSLayoutConstraint *constaint in windowActive.constraints) {
            if (constaint.firstItem == self.view) {
                if (constaint.firstAttribute == NSLayoutAttributeTop) {
                    constraintTopByWin = constaint;
                } else if (constaint.firstAttribute == NSLayoutAttributeLeft) {
                    constraintLeftByWin = constaint;
                } else if (constaint.firstAttribute == NSLayoutAttributeWidth) {
                    constraintWidthByWin = constaint;
                } else if (constaint.firstAttribute == NSLayoutAttributeHeight) {
                    constraintHeightByWin = constaint;
                }
            }
        }
        
        if (!constraintTopByWin) {
            // align self.view from the top
            constraintTopByWin = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:windowActive attribute:NSLayoutAttributeTop multiplier:1.0 constant:newRectToWindow.origin.y];
            [windowActive addConstraint:constraintTopByWin];
        }
        
        if (!constraintLeftByWin) {
            // align self.view from the left
            constraintLeftByWin = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:windowActive attribute:NSLayoutAttributeLeft multiplier:1.0 constant:newRectToWindow.origin.x];
            [windowActive addConstraint:constraintLeftByWin];
        }
        
        if (!constraintWidthByWin) {
            // self.view width constant
            constraintWidthByWin = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:NULL attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:newRectToWindow.size.width];
            [windowActive addConstraint:constraintWidthByWin];
        }
        
        if (!constraintHeightByWin) {
            // self.view height constant
            constraintHeightByWin = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:NULL attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:newRectToWindow.size.height];
            [windowActive addConstraint:constraintHeightByWin];
        }
        
        [windowActive setNeedsLayout];
        [windowActive layoutIfNeeded];
    }
    
    [UIView animateWithDuration:duration animations:^{
        CGRect bounds = [[UIScreen mainScreen] bounds];
        
        if (![self.view translatesAutoresizingMaskIntoConstraints]) {
            //view has autolayout
            constraintTopByWin.constant = 0.0;
            constraintLeftByWin.constant = 0.0;
            constraintWidthByWin.constant = bounds.size.width;
            constraintHeightByWin.constant = bounds.size.height;
            [windowActive setNeedsLayout];
            [windowActive layoutIfNeeded];
        } else {
            self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            self.view.frame = bounds;
        }
        
        _renderView.frame = [_renderView exactFrameRectForSize:self.view.bounds.size fillScreen:[self fillScreen]];
        [_renderView updateOpenGLFrameSizes];
        
    } completion:^(BOOL finished) {
        
        [topVc presentViewController:fsContainerVc animated:NO completion:^{
            [fsContainerVc.view addSubview:self.view];
            
            if (![self.view translatesAutoresizingMaskIntoConstraints]) {
                UIView *playerView = self.view;
                // align _playerController.view from the left and right
                [fsContainerVc.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[playerView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(playerView)]];
                
                // align _playerController.view from the top and bottom
                [fsContainerVc.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[playerView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(playerView)]];
            } else {
                self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
                self.view.frame = fsContainerVc.view.bounds;
            }
            
            _containerVc = [fsContainerVc retain];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kVKPlayerDidEnterFullscreenNotification object:nil userInfo:nil];
            _fullScreenAnimationInProgress = NO;
        }];
    }];
}

#pragma mark - FGThrowSliderDelegate

- (void)slider:(FGThrowSlider *)slider changedValue:(CGFloat)value {
    _durationCurrent = value;
    _labelStreamCurrentTime.text = [NSString stringWithFormat:@"%02d:%02d", (int)_durationCurrent/60, ((int)_durationCurrent % 60)];
}

- (void)slider:(FGThrowSlider *)slider endValue:(CGFloat)value {
    [self setStreamCurrentDuration:value];
    _labelStreamCurrentTime.text = [NSString stringWithFormat:@"%02d:%02d", (int)value/60, ((int)value % 60)];
    [self showControlPanel:YES willExpire:YES];
}

#pragma mark - Gesture Recognizers management

- (void)addScreenControlGesturesToContainerView:(UIView *)viewContainer renderView:(UIView *)viewRender {
    if (viewContainer) {
        _singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        _singleTapGestureRecognizer.numberOfTapsRequired = 1;
        _singleTapGestureRecognizer.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypeSelect]];
        _singleTapGestureRecognizer.delegate = self;
        [_view addGestureRecognizer:_singleTapGestureRecognizer];
    }
    if (viewRender) {
        _doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        _doubleTapGestureRecognizer.numberOfTapsRequired = 2;
        _doubleTapGestureRecognizer.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypeSelect]];
        _doubleTapGestureRecognizer.delegate = self;
        [_view addGestureRecognizer:_doubleTapGestureRecognizer];
        
        [_singleTapGestureRecognizer requireGestureRecognizerToFail:_doubleTapGestureRecognizer];
    }
}

- (void)addAppleTVRemoteControllerGestures {
    _homeTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(homeButtonTapped:)];
    _homeTapRecognizer.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypeMenu]];
    [_view addGestureRecognizer:_homeTapRecognizer];
    
    _pauseTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pauseButtonTapped:)];
    _pauseTapRecognizer.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypePlayPause]];
    [_view addGestureRecognizer:_pauseTapRecognizer];
}

- (void)removeScreenControlGesturesFromContainerView:(UIView *)viewContainer renderView:(UIView *)viewRender {
    if (_singleTapGestureRecognizer) {
        [_view removeGestureRecognizer:_singleTapGestureRecognizer];
        [_singleTapGestureRecognizer release];
        _singleTapGestureRecognizer = nil;
    }
    if (_doubleTapGestureRecognizer) {
        [_view removeGestureRecognizer:_doubleTapGestureRecognizer];
        [_doubleTapGestureRecognizer release];
        _doubleTapGestureRecognizer = nil;
    }
}

- (void)removeAppleTVRemoteControllerGestures {
    if (_homeTapRecognizer) {
        [_view removeGestureRecognizer:_homeTapRecognizer];
        [_homeTapRecognizer release];
        _homeTapRecognizer = nil;
    }
    if (_pauseTapRecognizer) {
        [_view removeGestureRecognizer:_pauseTapRecognizer];
        [_pauseTapRecognizer release];
        _pauseTapRecognizer = nil;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - Recording

#ifdef VK_RECORDING_CAPABILITY

- (void) startRecording {
    [super startRecording];
    _labelRecording.hidden = NO;
    timerForRecordingLabel = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                              target:self
                                                            selector:@selector(showHideRecordingLabel)
                                                            userInfo:nil
                                                             repeats:YES];
}

- (void) stopRecording {
    [super stopRecording];
    _labelRecording.hidden = YES;
    if (timerForRecordingLabel) {
        [timerForRecordingLabel invalidate];
        [timerForRecordingLabel release];
        timerForRecordingLabel = nil;
    }
}

- (void) showHideRecordingLabel {
    _labelRecording.hidden = !_labelRecording.hidden;
}

#endif

#pragma mark Timers callbacks

- (void)onTimerPanelHiddenFired:(NSTimer *)timer {
    [self showControlPanel:NO willExpire:YES];
}

- (void)onTimerElapsedFired:(NSTimer *)timer {
    [super onTimerElapsedFired:timer];
}

- (void)onTimerDurationFired:(NSTimer *)timer {
    
    if (_decoderState == kVKDecoderStatePlaying && !_slider.usesPanGestureRecognizer) {
        _durationCurrent = (_decodeManager) ? [_decodeManager currentTime] : 0.0;
        if (!isnan(_durationCurrent) && ((_durationTotal - _durationCurrent) > -1.0)) {
            _labelStreamCurrentTime.text = [NSString stringWithFormat:@"%02d:%02d", (int)_durationCurrent/60, ((int)_durationCurrent % 60)];
            _slider.value = _durationCurrent;
        }
    }
}


#pragma mark - Subview actions

- (void)showMenu {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Options"
                                                                   message:@"Select an action"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = nil;
    if (self.containerVc && ![self.containerVc isKindOfClass:[VKFullscreenContainer class]]) {
        UIAlertAction *closeAction = [UIAlertAction actionWithTitle:@"Close video" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {
                                                                [self stop];
                                                            }];
        [alert addAction:closeAction];
    } else {
        if (_fullScreen) {
            UIAlertAction *exitFullScreenAction = [UIAlertAction actionWithTitle:@"Exit Full Screen" style:UIAlertActionStyleDefault
                                                                         handler:^(UIAlertAction * action) {
                                                                             [self setFullScreen:NO];
                                                                         }];
            [alert addAction:exitFullScreenAction];
            
        } else {
            cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:NULL];
            [alert addAction:cancelAction];
        }
    }
    
    if (_readyToApplyPlayingActions) {
        NSString *muteTitle = _mute ? @"Unmute": @"Mute";
        UIAlertAction *muteAction = [UIAlertAction actionWithTitle:muteTitle style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               [self setMute:!_mute];
                                                           }];
        [alert addAction:muteAction];
        
        NSString *zoomTitle = _scrollView.zoomScale <= 1.0 ? @"Zoom In" : @"Zoom Out";
        UIAlertAction *zoomAction = [UIAlertAction actionWithTitle:zoomTitle style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               [self zoomInOut:nil];
                                                           }];
        [alert addAction:zoomAction];
        
#ifdef VK_RECORDING_CAPABILITY
        if (![_decodeManager recordingNow]) {
            UIAlertAction *startRecordingAction = [UIAlertAction actionWithTitle:@"Start Recording" style:UIAlertActionStyleDefault
                                                                         handler:^(UIAlertAction * action) {
                                                                             [self startRecording];
                                                                         }];
            [alert addAction:startRecordingAction];
        } else {
            UIAlertAction *stopRecordingAction = [UIAlertAction actionWithTitle:@"Stop Recording" style:UIAlertActionStyleDefault
                                                                        handler:^(UIAlertAction * action) {
                                                                            [self stopRecording];
                                                                        }];
            [alert addAction:stopRecordingAction];
        }
#endif
        
    }
    
    NSString *infoTitle = _viewInfo.hidden ? @"Show Info View" : @"Hide Info View";
    UIAlertAction *infoAction = [UIAlertAction actionWithTitle:infoTitle style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           if(_viewInfo.hidden){
                                                               [self performSelector:@selector(showInfoView)];
                                                           } else {
                                                               _viewInfo.hidden=YES;
                                                           }
                                                       }];
    [alert addAction:infoAction];
    
    if(!cancelAction) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Close Menu" style:UIAlertActionStyleCancel handler:NULL];
        [alert addAction:cancelAction];
    }
    
    if (self.containerVc) {
        [self.containerVc  presentViewController:alert animated:YES completion:nil];
    } else {
        UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        while (topVC.presentedViewController) {
            topVC = topVC.presentedViewController;
        }
        [topVC presentViewController:alert animated:YES completion:nil];
    }
}

- (void)showControlPanel:(BOOL)show willExpire:(BOOL)expire {
    
    if (_controlStyle == kVKPlayerControlStyleTVNone) {
        float alpha = 0.0;
        _viewBottomPanel.alpha = alpha;
        _viewTopPanel.alpha = alpha;
        
        return;
    }
    
    if (!show && [_slider usesPanGestureRecognizer]) {
        goto retry;
    }
    
    _panelIsHidden = !show;
    
    if (_timerPanelHidden && [_timerPanelHidden isValid]) {
        [_timerPanelHidden invalidate];
    }
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionTransitionNone
                     animations:^{
                         CGFloat alpha = _panelIsHidden ? 0 : 1;
                         
                         _viewBottomPanel.alpha = alpha;
                         _viewTopPanel.alpha = alpha;
                         
                     }
                     completion:^(BOOL finished) {
                         if (_panelIsHidden) {
                             [self focusOnSubview:NO];
                         }
                     }];
    
retry:
    if (!_panelIsHidden && expire) {
        [_timerPanelHidden release];
        _timerPanelHidden = nil;
        _timerPanelHidden = [[NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(onTimerPanelHiddenFired:) userInfo:nil repeats:NO] retain];
    }
}


#pragma mark - VKDecoder delegate methods

- (void)decoderStateChanged:(VKDecoderState)state errorCode:(VKError)errCode {
    _decoderState = state;
    if (state == kVKDecoderStateConnecting) {
        _readyToApplyPlayingActions = NO;
        _imgViewAudioOnly.hidden = YES;
        _slider.value = 0.0;
        _labelTopTitle.text = TR(@"Loading...");
        [_activityIndicator startAnimating];
        [self showControlPanel:YES willExpire:NO];
        _snapshotReadyToGet = NO;
        VKLog(kVKLogLevelStateChanges, @"Trying to connect to %@", _contentURLString);
    } else if (state == kVKDecoderStateConnected) {
        VKLog(kVKLogLevelStateChanges, @"Connected to the stream server");
    } else if (state == kVKDecoderStateInitialLoading) {
        _readyToApplyPlayingActions = YES;
        VKLog(kVKLogLevelStateChanges, @"Trying to get packets");
    } else if (state == kVKDecoderStateReadyToPlay) {
        VKLog(kVKLogLevelStateChanges, @"Got enough packets to start playing");
        [_activityIndicator stopAnimating];
        _labelTopTitle.text =  [self barTitle];
        [self showControlPanel:NO willExpire:NO];
    } else if (state == kVKDecoderStateBuffering) {
        VKLog(kVKLogLevelStateChanges, @"Buffering now...");
    } else if (state == kVKDecoderStatePlaying) {
        VKLog(kVKLogLevelStateChanges, @"Playing now...");
        _snapshotReadyToGet = YES;
    } else if (state == kVKDecoderStatePaused) {
        VKLog(kVKLogLevelStateChanges, @"Paused now...");
    } else if (state == kVKDecoderStateGotStreamDuration) {
        if (errCode == kVKErrorNone) {
            _slider.hidden = NO;
            _labelStreamCurrentTime.hidden = NO;
            _labelStreamTotalDuration.hidden = NO;
            [self focusOnSubview:YES];
            
            _durationTotal = [_decodeManager durationInSeconds];
            VKLog(kVKLogLevelDecoder, @"Got stream duration: %f seconds", _durationTotal);
            _slider.maximumValue = _durationTotal;
            _labelStreamTotalDuration.text = [NSString stringWithFormat:@"%02d:%02d", (int)_durationTotal/60, ((int)_durationTotal % 60)];
            if (_initialPlaybackTime > 0.0 && _initialPlaybackTime < _durationTotal) {
                _durationCurrent = _initialPlaybackTime;
            }
            [self startDurationTimer];
        } else {
            VKLog(kVKLogLevelDecoder, @"Stream duration error -> %@", errorText(errCode));
        }
        [self updateBarWithDurationState:errCode];

    } else if (state == kVKDecoderStateGotAudioStreamInfo) {
        if (errCode != kVKErrorNone) {
            VKLog(kVKLogLevelStateChanges, @"Got audio stream error -> %@", errorText(errCode));
        }
    } else if (state == kVKDecoderStateGotVideoStreamInfo) {
        if (errCode != kVKErrorNone) {
            _imgViewAudioOnly.hidden = NO;
            VKLog(kVKLogLevelStateChanges, @"Got video stream error -> %@", errorText(errCode));
        }
    } else if (state == kVKDecoderStateConnectionFailed) {
        if (_controlStyle == kVKPlayerControlStyleTVDefault) {
            NSString *title = TR(@"Error: Stream can not be opened");
            NSString *body = errorText(errCode);
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:body preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *closeAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                                handler:^(UIAlertAction * action) {
                                                                    if (self.containerVc && ![self.containerVc isKindOfClass:[VKFullscreenContainer class]]) {
                                                                        [self stop];
                                                                    }
                                                                }];
            [alert addAction:closeAction];
            [self.containerVc presentViewController:alert animated:YES completion:nil];
        }
        
        _readyToApplyPlayingActions = NO;
        _labelTopTitle.text = TR(@"Connection error");
        [self stopElapsedTimer];
        [self stopDurationTimer];
        
        [_activityIndicator stopAnimating];
        [self updateBarWithDurationState:kVKErrorOpenStream];
        
        [self updateBarWithDurationState:kVKErrorOpenStream];
        VKLog(kVKLogLevelStateChanges, @"Connection error - %@",errorText(errCode));
    } else if (state == kVKDecoderStateStoppedByUser) {
        _readyToApplyPlayingActions = NO;
        [self stopElapsedTimer];
        [self stopDurationTimer];
        [self updateBarWithDurationState:kVKErrorStreamReadError];
        [_activityIndicator stopAnimating];
        VKLog(kVKLogLevelStateChanges, @"Stopped now...");
    } else if (state == kVKDecoderStateStoppedWithError) {
        _readyToApplyPlayingActions = NO;
        if (errCode == kVKErrorStreamReadError) {
            if (_controlStyle == kVKPlayerControlStyleTVDefault) {
                NSString *title = TR(@"Error: Read error");
                NSString *body = errorText(errCode);
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:body preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *closeAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                                    handler:^(UIAlertAction * action) {
                                                                        if (self.containerVc && ![self.containerVc isKindOfClass:[VKFullscreenContainer class]]) {
                                                                            [self stop];
                                                                        }
                                                                    }];
                [alert addAction:closeAction];
                [self.containerVc presentViewController:alert animated:YES completion:nil];
            }
            _labelTopTitle.text = TR(@"Error: Read error");
            VKLog(kVKLogLevelStateChanges, @"Player stopped - %@",errorText(errCode));
        } else if (errCode == kVKErrorStreamEOFError) {
            VKLog(kVKLogLevelStateChanges, @"%@, stopped now...", errorText(errCode));
        }
        [self stopElapsedTimer];
        [self stopDurationTimer];
        
        [_activityIndicator stopAnimating];
        [self updateBarWithDurationState:errCode];
        
    }
    if(_delegate && [_delegate respondsToSelector:@selector(player:didChangeState:errorCode:)]) {
        [_delegate player:self didChangeState:state errorCode:errCode];
    }
}

#pragma mark - Gesture Recognizers handling

- (void)homeButtonTapped:(UITapGestureRecognizer *)recognizer {
    [self showMenu];
}

- (void)pauseButtonTapped:(UITapGestureRecognizer *)recognizer {
    [self togglePause];
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    if (recognizer.numberOfTapsRequired == 1) {
        BOOL updateFocus = NO;
        if ([_view isFocused]) {
            [self showControlPanel:YES willExpire:YES];
            updateFocus = YES;
        } else {
            [self showControlPanel:_panelIsHidden willExpire:YES];
            if (!_panelIsHidden) {
                updateFocus = YES;
            }
        }
        
        if (updateFocus) {
            [self focusOnSubview:YES];
        }
    } else {
        [self setFullScreen:!_fullScreen];
    }
}

#pragma mark - External Screen Management (Cable & Airplay)

- (void)screenDidChange:(NSNotification *)notification {
    //no airplay feature
}

#pragma mark - Memory events & deallocation

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeAppleTVRemoteControllerGestures];
    
#ifdef VK_RECORDING_CAPABILITY
    [_labelRecording release];
#endif
    [_viewInfo release];
    [_slider setDelegate:nil];
    [_slider release];
    
    [super dealloc];
}

@end
